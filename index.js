import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import router from "./src/route/route";
import Sequelize from "sequelize";
import User from "./src/models/User";
import Recipe from "./src/models/Recipe";
import Instruction from "./src/models/Instruction";
import Ingredient from "./src/models/Ingredient";
import database from "./src/models/database";


const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(cors({origin:true}));

app.use(router);
const port = 3010;

// const sequelize = new Sequelize('feedie', 'root', '', {
//   host: 'localhost',
//   dialect: 'mysql',
// });

// sequelize
//   .authenticate()
//   .then(() => {
//     console.log('Connection has been established successfully.');
//   })
//     .then(() => {
//         // sequelize.sync();
//     })
//   .catch(err => {
//     console.error('Unable to connect to the database:', err);
//   });



database.connectDb().then(() => {
    console.log('nosql Database server is connected...');
    app.listen(port, () => {
        console.log(`Server listening on port ${port}`);
    })
});