import Ingredient from "../models/Ingredient";
import User from "../models/User";
import Recipe from "../models/Recipe";

class IngredientController{
    /**
     * function to register a new ingredient
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async create(req,res){
        let status = 200;
        let body = {};

        try {
            console.log(req.body);
            let ingredient = await Ingredient.create({
                ingredient: req.body.ingredient,
                recipeId: req.body.recipeId
            });
            let addToRecipe = await Recipe.findOneAndUpdate(
                {_id: req.body.recipeId},
                {
                    $push: {
                        ingredient: {
                            ingredientId: ingredient._id
                        }
                    }
                },
                {new: true});
            body = {
                'message': `Ingredient ${ingredient.ingredient} created`,
                ingredient,
                addToRecipe
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function to delete current user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let ingredient = await Ingredient.findByIdAndRemove(id);
            //await User.remove({_id: req.params.id});
            body = {
                'message': `ingredient ${ingredient.ingredient} (id: ${ingredient._id} deleted`
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    /**
     * function to update current user informations
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            let ingredient = await Ingredient.findById(req.params.id);
            console.log(req.params.id);
            await ingredient.update({
                ingredient: req.body.ingredient
            });

            body = {
                'message': `ingredient updated`,
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
}

export default IngredientController;