import { DataTypes } from "sequelize";
import Recipe from "../models/Recipe";
import Favorite from "../models/Favorite";
import User from "../models/User";
import Ingredient from "../models/Ingredient";


class RecipeController {
        /**
     * function to find all user created from the DB
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async findDummy(req, res){
        let status = 200;
        const fakeDataRecipe = {
            recipes: [
                {id: 0, name: 'recette n°1', description: 'description du plat n°1', preparation_time: 55, visual: 'recipe.jpg', creation_date: '25/02/2020 10:10:10', like_number: 0, category: 'plat chaud'},
                {id: 1, name: 'recette n°2', description: 'description du plat n°2', preparation_time: 55, visual: 'recipe.jpg', creation_date: '25/02/2020 10:10:10', like_number: 0, category: 'plat chaud'},
                {id: 2, name: 'recette n°3', description: 'description du plat n°3', preparation_time: 55, visual: 'recipe.jpg', creation_date: '25/02/2020 10:10:10', like_number: 0, category: 'plat chaud'},
                {id: 3, name: 'recette n°4', description: 'description du plat n°4', preparation_time: 55, visual: 'recipe.jpg', creation_date: '25/02/2020 10:10:10', like_number: 0, category: 'plat chaud'},
                {id: 4, name: 'recette n°5', description: 'description du plat n°5', preparation_time: 55, visual: 'recipe.jpg', creation_date: '25/02/2020 10:10:10', like_number: 0, category: 'plat chaud'},
                {id: 5, name: 'recette n°6', description: 'description du plat n°6', preparation_time: 55, visual: 'recipe.jpg', creation_date: '25/02/2020 10:10:10', like_number: 0, category: 'plat chaud'},
                {id: 6, name: 'recette n°7', description: 'description du plat n°7', preparation_time: 55, visual: 'recipe.jpg', creation_date: '25/02/2020 10:10:10', like_number: 0, category: 'plat chaud'},
            ]
        };


        return res.status(status).json(fakeDataRecipe);
    }
        /**
     * function to register a new user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async create(req,res){
        let status = 200;
        let body = {};

        try {
            console.log(req.body);
            let recipe = await Recipe.create({
                name: req.body.name,
                description: req.body.description,
                preparation_time: req.body.preparation_time,
                visual: req.body.visual,
                creation_date: req.body.creation_date,
                like_numbers: req.body.like_numbers,
                category: req.body.category,
                creator: req.body.creator
            });
            body = {
                'message': `Recipe ${recipe.name} created`,
                recipe
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function to find all user created from the DB
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async findAll(req, res){
        let status = 200;
        let body = {};

        try {
            let recipe = await Recipe.find();
            body = {
                recipe,
                'message': 'Recipe list'
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    /**
     * function to delete current user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async search(req, res){
        let status = 200;
        let body = {};

        try {
            let name = req.body.name;
            let recipes = await Recipe.find({name: name});
            body = {
                'message': `Recipes found `,
                recipes
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    /**
     * function to find all user created from the DB
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async findFavorites(req, res){
        let status = 200;
        let body = {};

        try {
            let recipe = await Favorite.find();
            body = {
                recipe,
                'message': 'Favorites recipe list'
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }


    /**
     * function to register a new user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async addFavorite(req,res){
        let status = 200;
        let body = {};

        try {
            console.log(req.body);
            let recipe = await Favorite.create({
                recipeId: req.body.recipeId,
                userId: req.body.userId
            });
            body = {
                'message': `Favorite added`,
                recipe
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }


    /**
     * function to register a new user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async addIngredient(req,res){
        let status = 200;
        let body = {};

        try {
            console.log(req.body);
            let currentIngredient = await Ingredient.findById(req.body.ingredientId);
            let currentRecipeId = await Recipe.findById(req.body.recipeId);
            let recipe = await Recipe.findOneAndUpdate(
                { _id: currentRecipeId._id },
                {
                    $push: {
                        ingredient:{
                            ingredientId: currentIngredient
                        }
                    }
                },
                {new: true});
            body = {
                'message': `Favorite added`,
                recipe
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }


    /**
     * function to register a new user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async deleteFavorite(req,res){
        let status = 200;
        let body = {};

        try {
            console.log(req.body);
            let id = req.params.id;
            let recipe = await Favorite.findByIdAndRemove(id);
            body = {
                'message': `Favorite deleted`,
                recipe
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function to update current user informations
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            let recipe = await Recipe.findById(req.params.id);
            console.log(req.params.id);
            await recipe.update({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                password: req.body.password,
                avatar: req.body.avatar
            });

            body = {
                'message': `recipe n° ${recipe._id} updated`,
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }


/**
     * function to delete current user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let recipe = await Recipe.findByIdAndRemove(id);
            //await User.remove({_id: req.params.id});
            body = {
                'message': `recipe ${recipe.name} (id: ${recipe._id} deleted`
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }
}

export default RecipeController;