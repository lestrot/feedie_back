import { DataTypes } from "sequelize";
import Recipe from "../models/Recipe";
import User from "../models/User";


class UserController {
    /**
     * function to register a new user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async create(req,res){
        let status = 200;
        let body = {};

        try {
            console.log(req.body);
            let user = await User.create({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                password: req.body.password,
                avatar: req.body.avatar
            });
            body = {
                'message': `User ${user.firstname} created`,
                user
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function to display all user current information
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let user = await User.findById(id);
            body = {
                'message': `Details from ${user.firstname}`,
                user
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    /**
     * function to update current user informations
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            let user = await User.findById(req.params.id);
            console.log(req.params.id);
            await user.update({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                password: req.body.password,
                avatar: req.body.avatar
            });

            body = {
                'message': `profile updated`,
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    /**
     * function to delete current user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let user = await User.findByIdAndRemove(id);
            //await User.remove({_id: req.params.id});
            body = {
                'message': `user ${user.firstname} (id: ${user._id} deleted`
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }


    /**
     * function to delete current user
     * @param req
     * @param res
     * @returns {Promise<Json|any>}
     */
    static async login(req, res){
        let status = 200;
        let body = {};

        try {
            let email = req.body.email;
            let password = req.body.password;
            let user = await User.find({email: email});
            let loginOK = email === user[0].email;
            let passwordOK = password === user[0].password;
            if (loginOK && passwordOK){
                body = {
                'message': `ok, proceed`,
                user
                };
            }else{
                status = 403;
                body = {
                'message': `login or password not ok`
                };
            }

        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }


}

export default UserController;