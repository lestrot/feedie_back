import mongoose from 'mongoose';

const favoriteSchema = new mongoose.Schema({
    recipeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Recipe'
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

const Favorite = mongoose.model('Favorite', favoriteSchema);
export default Favorite;