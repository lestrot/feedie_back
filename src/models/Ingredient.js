// import { Sequelize, Model, DataTypes } from "sequelize";
// import Recipe from "./Recipe";
// const sequelize = new Sequelize('feedie', 'root', '', {
//   host: 'localhost',
//   dialect: 'mysql',
// });
//
// class Ingredient extends Model {}
// Ingredient.init({
//   recipe_id: {
//     type: DataTypes.INTEGER,
//     allowNull: false,
//     references: {
//       model: 'recipes',
//       key: 'id'
//     }
//   },
//   ingredient: {
//     type: DataTypes.STRING,
//     allowNull: false
//   }
// }, {
//   sequelize,
//   modelName: 'ingredients'
// });
//
// export default Ingredient;

import mongoose from 'mongoose';

const ingredientSchema = new mongoose.Schema({
  ingredient: {
    type: String,
    required: true
  },
  recipeId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Recipe'
  }

});

const Ingredient = mongoose.model('Ingredient', ingredientSchema);
export default Ingredient;