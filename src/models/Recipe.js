// import { Sequelize, Model, DataTypes } from "sequelize";
// const sequelize = new Sequelize('feedie', 'root', '', {
//   host: 'localhost',
//   dialect: 'mysql',
// });
//
// class Recipe extends Model {}
// Recipe.init({
//   name: {
//     type: DataTypes.STRING,
//     allowNull: false
//   },
//   description: {
//     type: DataTypes.TEXT,
//   },
//   preparation_time: {
//     type: DataTypes.INTEGER,
//     allowNull: false
//   },
//   visual: {
//     type: DataTypes.STRING,
//   },
//   creation_date: {
//     type: DataTypes.DATE,
//     defaultValue: DataTypes.NOW,
//     allowNull: false
//   },
//   like_numbers: {
//     type: DataTypes.INTEGER,
//     defaultValue: 0
//   },
//   category: {
//     type: DataTypes.STRING,
//     allowNull: false
//   }
// }, {
//   sequelize,
//   modelName: 'recipes'
//   // options
// });
//
// export default Recipe;

import mongoose from 'mongoose';

const recipeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    preparation_time: {
        type: Number,
        required: true
    },
    visual: {
        type: String,
        default: 'recipe.jpg'
    },
    creation_date: {
        type: Date,
        required: true,
        default: Date.now()
    },
    like_numbers: {
        type: Number,
        default: 0,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    ingredient: [
        {
            ingredientId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Ingredient'
            }
        }
    ],
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

const Recipe = mongoose.model('Recipe', recipeSchema);
export default Recipe;